## Contents
This repo contains code that can be used to model the dynamics of a communicable disease moving through a population separated into groups of fixed size, subject to various intervention strategies that are designed to identify and isolate infected individuals and thereby combat the spread of the disease.

Additionally, this repo contains analysis notebooks that use parameterized models to perform simulations and generate figures that explore the impact of various intervention policies on the spread of SARS-CoV-2 through members of a university community that is subject to one or more active and passive mitigation strategies. 

## Model Summary
We model the daily health status (susceptible, exposed, infected, or recovered), and the isolation and quarantine status of a population of individuals that is partitioned into one or more mutually exclusive groups of fixed size (e.g., undergraduates on campus, faculty, staff, etc.) as the infection spreads through the population. The model is best described as a stochastic, agent-based, modified SEIR-model in which several dynamic attributes of each member of a population are updated daily and tracked over time.

Each day some number of randomly chosen susceptible agents within each group will become exposed to the disease at rates determined by within and between group interaction parameters and the current prevalence of the disease within those groups. The rates of disease transmission are determined by two asymmetrical group-pair interaction parameters which quantify the average number of interactions between members of the groups and the probability that such an interaction will lead to a transmission.  Additional exposures may also occur due to interactions of agents with the surrounding population (i.e., the outside community), whose disease prevalence is specified at the aggregate level and is not influenced by the disease dynamics of the modelled population. 

Also, each day the health status of some number of exposed individuals changes to infected/detectable and some number of agents move from infected to recovered in accordance with those individal's disease progression parameters which are randomly assigned and fixed for each agent. The model does not explicitly track disease severity or death and it is assumed that exposed individuals are neither detectable nor infectious.
On any day, an agent may be put into or removed from quarantine or isolation. The effect is to remove that individual from the model of interactions with the other agents and the surrounding population. An agent's health status and their quarantine status are not coupled, allowing the model to flexibly and realistically model various intervention strategies. 

The model is designed to be easily extensible to new types of mitigation strategies. Currently the model implements multiple types of testing logics with tunable parameters, contact-tracing protocols, and sheltering-in-place/lockdown strategies. These interventions act in various ways to reduce the spread of the disease, including by modifying the assumed within-group interaction parameters and by isolating individuals and preventing them from interacting with others.  

The model supports several specific intervention strategies that emulate the interventions and policies adopted by many US universities. For example, the model supports symptomatic testing as well as pooled surveillance testing in which each agent is tested at a fixed frequency, with tests first being pooled into groups and subsequent tests being performed on individual samples in positive pools. Also, although individual interactions are not explicitly modelled, the model supports a data-driven emulation of contact-tracing with variable efficiency.

## Model Details
### Agent Attributes 
The Population class represents a collection of agents partitioned into groups of fixed size. Each agent is endowed with the following attributes which are either fixed in time (having been randomly assigned at the initialization of a population) or may change each day:

* agents_states (variable) - String specifying agent's health state ('s', 'e', 'i', or 'r')
* susceptible (variable) - Flag indicating if agent is susceptible to infection
* exposed (variable) - Flag indicating if agent is exposed and will become infected
* infected (variable) - Flag indicating if agent is infected
* recovered (variable) - Flag indicating if agent has recovered from infection
* quarantined (variable) - Flag indicating if agent is quarantined
* interactable (variable) - Flag indicating if an agent can or cannot interact with other agents and the outside community
* agents_days_in_quarantine (variable) - The number of days the agent has been in quarantine
* agents_days_until_quarantine_release (variable) - A countdown timer tracking the number of days until the agent is released from quarantine
* isolated (variable) - Flag indicating if agent is isolated
* agents_days_until_isolation (variable) - A countdown timer tracking the number of days until an agent is sent into isolation due to a positive test
* agents_days_in_isolation (variable) - The number of days the agent has been in isolation
* agents_days_until_isolation_release (variable) - A countdown timer tracking the number of days until the agent is released from isolation
* agents_days_since_exposed (variable) - The number of days since the agent was first exposed
* agents_days_since_test (variable) - The number of days since the agent was last tested
* testable (variable) - Flag indicating if the agent can be tested
* agents_days_in_state (variable) - the number of days the agent has been in the current state
* agents_duration_of_exposed (fixed) - The numbers of days the agents stay in the exposed state 
* agents_duration_of_infected (fixed) - The numbers of days the agents stay in the infected state 
* agents_will_be_symptomatic (fixed) - A flag indicating whether the agent will have a symptomatic infection or not
* agents_delay_until_symptomatic (fixed) - The number of days after becoming infected until the agent becomes symptomatic (if they do)
* not_infected_with_symptoms (variable) - A flag indicating if an uninfected agent has symptoms (due to some other illness)
* has_symptoms (variable) - A flag indicating if an agent has symptoms (whether due to infection or otherwise)
* agents_number_of_contacts_reported (fixed) - The number of contacts the agent will report if they test positive

Each of the above attributes is stored as an ordered numpy array whose i-th entry represents the state or value of that attribute for the i-th agent. 

### Population Attributes
In addition to storing agent attributes, the Population class has several population/system-level attributes. Some of these attributes are fixed in the sense that they do not depend on the dynamics of disease but are adjustable from day to day for more complicated models. 

* outside_prevalence (fixed, adjustable) - The current prevalence of disease in the outside community
* outside_exposure_probability (fixed, adjustable) - The current probability that an interaction between an agent and the outside community will result in an exposures may also occur due to interactions of agents with the surrounding population
* max_isolation_duration (fixed) - The number of days an agent is kept in isolation unless released early for some other reason (Default: 10 days)
* max_quarantine_duration (fixed) - The number of days an agent is kept in quarantine unless released early for some other reason (Default: 10 days)
* quarantine_not_interactable (fixed) - A flag determining if contacts ("quarantined" agents) are isolated from the rest of the population or not
* number_tests_performed (variable) - The running total number of tests performed on agents
* number_positive_tests (variable) - The daily count of positive tests 
* number_exposed_from_outside (variable) - The running total count of exposures due to interactions with the outside community
* number_exposed_from_inside (variable) - The running total count of exposures due to interactions between agents
* under_lockdown (fixed, adjustable) - A flag tracking which groups in the population are currently under lockdown strategies
* time_series_df (variable) - A flat pandas dataframe tracking counts of agents with various attributes across days and groups

### Population Methods
The Population class defines several methods that allow for 

* perform_tests(testing_strategy, param_dict) - Enables simple invocation of a testing strategy function (see ```src/model/strategies.py```)
* trace_contacts(trace_strategy, param_dict) - Enables simple invocation of a contact tracing strategy function (see ```src/model/strategies.py```)
* release_agents(release_strategy, param_dict) - Enables simple invocation of a release from isolation/quarantine strategy function (none currently implemented)
* isolate_agents - Isolates those agents that had a positive test when the test results become available
* release_from_isolation - Release those agents whose isolation countdown has reached 0
* release_from_quarantine - Release agents whose quarantine countdown has reached 0
* spike_exposures(self, num_exposures=None) - A function enabling the addition of a specified number of exposures into each group
* activate_lockdown(groups, interaction_reduction_percentage) - Lockdown one or more groups with a specified efficacy
* deactivate_lockdown(groups) - Release one or more groups from lockdown
* step - Increment one day forward in time, creating new exposures due to interactions, updating disease progression, isolating, quarantining, and releasing agents

### Agent Attribute Distributions
Fixed agent attributes are randomly assigned from user-specified distributions during the initialization of a Population object. The default distributions are defined in ```src/model/distributions.py```:

* duration_of_exposed_sample - The number of days agents stay in the exposed state 
* duration_of_infected_sample - The number of days agents stay in the infected state
* symptomatic_infection - A flag determining if an agent will develop a symptomatic infection or not
* symptomatic_infection_delay - The number of days after infection that an agent develops symptoms
* number_of_contacts_reported - The number of contacts an agent will report if they test positive

### Mitigation Strategies

To match the policies of many US Universities and emulate common mitigation strategies during the SARS-CoV-2 pandemic, this model supports quarantining or isolating agents according to the following logic: An agent is isolated if they test positive when the test result becomes available, while an agent is quarantined if they are identified as a contact of an agent who has tested positive.  By default, isolation and quarantine has the same effect: the agent is no longer interactable and thus cannot become exposed, nor do they contribute to the chance of any other agents becoming exposed. However, to facilitate other protocols for handling agents identified as contacts, the attribute Population.quarantine_not_interactable can be set to False, in which case contacts (still referred to as in the "quarantined" state) remain interactable. 

To study the impact of various testing, contact-tracing, and release from isolation/quarantine protocols on disease dynamics, the model is designed to be highly extensible and flexible in the scope and logic of possible mitigation strategies. 

* A testing strategy is a function which takes as input a Population object and some set of parameters and at least updates the following Population attributes:
    - population.agents_days_since_test
    - population.agents_days_until_isolation
    - population.number_tests_performed
* A contact tracing strategy is a function which takes as input a Population object and some set of parameters and at least updates the following Population attributes:
    - population.quarantined
    - population.agents_days_in_quarantined
* A release strategy is a function which takes as input a Population object and some set of parameters and at least updates the following Population attributes:
    - population.quarantined and/or population.isolated
    - population.agents_days_in_quarantined and/or population.agents_days_in_insolation
	
See ```src/model/strategies.py``` for the current set of implemented strategies. 

## Installation Guide

It is assumed that you have conda installed (See https://conda.io). To install the model and open a jupyter notebook to run the included notebooks, simply execute the following commands in terminal:

```
git clone git@gitlab.com:duke-covid-modeling/covid_campus_interventions.git
cd covid_campus_interventions
conda env create -f conda_env.yml
conda activate covid-model
jupyter notebook
```

Output from `simulations/notebooks/covid_campus_interventions_simulations.ipynb` have been precomputed and compressed to `simulations/data/simulation_data.tar.gz`. Rerun `covid_campus_interventions_simulations.ipynb` or unpack prior to running `figures_and_tables.ipynb`.
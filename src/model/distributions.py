"""
Distributions defining disease behaviors.
"""

import numpy as np
import pandas as pd


def lognormal_sample(n, mean, stdev, minimum):
    """
    Draws a sample of integers gotten by rounding samples from a log-normal distribution having any sample below
    a specified minimum replaced with that minimum

    Parameters
    ----------
    n : int >= 1
        The number of samples to draw
    mean : float
        The desired mean of the log-normal
    stdev : float >= 1
        The desired standard deviation of the log normal
    minimum : int >= 0
        The minimum integer value allowed
    """
    return np.maximum(minimum * np.ones(n),
                      np.round(np.random.lognormal(mean=np.log(mean ** 2 / np.sqrt(mean ** 2 + stdev ** 2)),
                                                   sigma=np.sqrt(np.log(1 + (stdev / mean) ** 2)),
                                                   size=n))).astype(int)


def duration_of_exposed_sample(n):
    """
    The number of days agents stay in the exposed state.

    Parameters
    ----------
    n : int >= 1
        The number of samples to draw, i.e. the number of agents in a group

    Returns
    -------
    An (n,)-numpy array of ints drawn from a modified log normal distribution
    """
    return lognormal_sample(n, mean=4.6, stdev=4.8, minimum=3)


def duration_of_infected_sample(n):
    """
    The number of days agents stay in the infected state.

    Parameters
    ----------
    n : int >= 1
        The number of samples to draw, i.e. the number of agents in a group

    Returns
    -------
    An (n,)-numpy array of ints drawn from two modified log normal distribution
    """
    return np.concatenate((lognormal_sample(int(np.floor(0.011 * n)), mean=14.0, stdev=2.4, minimum=1),
                           lognormal_sample(int(np.ceil(0.989 * n)), mean=8.0, stdev=2.0, minimum=1)))


def symptomatic_infection(n):
    """
    A flag determining if an agent will develop a symptomatic infection or not.

    Parameters
    ----------
    n : int >= 1
        The number of samples to draw, i.e. the number of agents in a group

    Returns
    -------
    An (n,)-numpy boolean array
    """
    return np.random.choice([True, False], size=(n,), p=[0.57, 0.43])


def symptomatic_infection_delay(n):
    """
    The number of days post-infection that an agent develops symptoms.

    Parameters
    ----------
    n : int >= 1
        The number of samples to draw, i.e. the number of agents in a group

    Returns
    -------
    An (n,)-numpy array of ints drawn from a modified log normal distribution
    """
    return lognormal_sample(n, mean=1, stdev=1, minimum=0)


def empirical_contacts_cdf(contacts_pdf_file=None):
    """
    An empirical cumulative distribution of reported contacts.

    Parameters
    ----------

    contacts_pdf_file : string
        The path to a tab-delimited file encoding a frequency probability distribution of reported contacts

    Returns
    -------
    contact_cdf : The empirical cumulative distribution of reported contacts
    """
    contacts_pdf_df = pd.read_csv(contacts_pdf_file, comment='#', sep='\t')
    contact_cdf = np.cumsum(contacts_pdf_df['frequency'] / contacts_pdf_df['frequency'].sum())

    return contact_cdf


def number_of_contacts_reported(n, contact_cdf=None):
    """
    The number of contacts an agent will report if they test positive.

    Parameters
    ----------
    n : int >= 1
        The number of samples to draw, i.e. the number of agents in a group
    contact_cdf : numpy array
        An empirical cumulative distribution of reported contacts output by empirical_contacts_cdf()

    Returns
    -------
    contacts : An (n,)-numpy array of ints drawn from the empirical contact distribution
    """
    contacts = np.zeros(n).astype(int)
    for i in range(n):
        contacts[i] = np.argmax(contact_cdf >= np.random.uniform(0, 1))
    return contacts

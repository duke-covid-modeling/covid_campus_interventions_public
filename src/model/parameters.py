"""
Function to help load predefined parameters
"""

import json
import pandas as pd
import model.distributions as dists


def load_from_file(config_json):
    # load configuation file
    with open(config_json) as json_file:
        config_data = json.load(json_file)

    # construct functions which generate random samples that control disease and agent behaviors
    duration_of_exposed = {group: dists.duration_of_exposed_sample for group in config_data["groups"]}
    duration_of_infected = {group: dists.duration_of_infected_sample for group in config_data["groups"]}
    symptomatic_infection = {group: dists.symptomatic_infection for group in config_data["groups"]}
    symptomatic_infection_delay = {group: dists.symptomatic_infection_delay for group in config_data["groups"]}

    contact_cdf = dists.empirical_contacts_cdf(config_data['contacts_pdf_file'])
    number_of_contacts_reported = {group: lambda n: dists.number_of_contacts_reported(n, contact_cdf=contact_cdf)
                                   for group in config_data["groups"]}

    # compute probabilities of exposures from outside interactions
    outside_exposure_probability = {}
    for group in config_data["groups"]:
        outside_exposure_probability[group] = config_data["outside_prevalence"] * \
                                                 config_data["outside_interaction_exposure_prob"][group] * \
                                                 config_data["outside_daily_interactions"][group]

    population_parameters = {'groups': config_data["groups"],
                             'sizes': config_data["sizes"],
                             'daily_interactions': config_data['daily_interactions'],
                             'interaction_exposure_prob': config_data['interaction_exposure_prob'],
                             'outside_prevalence': config_data["outside_prevalence"],
                             'outside_daily_interactions': config_data["outside_daily_interactions"],
                             'outside_interaction_exposure_prob': config_data["outside_interaction_exposure_prob"],
                             'infected_prevalence': config_data["infected_prevalence"],
                             'exposed_prevalence': config_data["exposed_prevalence"],
                             'recovered_prevalence': config_data["recovered_prevalence"],
                             'duration_of_exposed': duration_of_exposed,
                             'duration_of_infected': duration_of_infected,
                             'symptomatic_infection': symptomatic_infection,
                             'uninfected_daily_symptom_probability': config_data["uninfected_daily_symptom_probability"],
                             'outside_exposure_probability': outside_exposure_probability,
                             'symptomatic_infection_delay': symptomatic_infection_delay,
                             'number_of_contacts_reported': number_of_contacts_reported}

    return population_parameters

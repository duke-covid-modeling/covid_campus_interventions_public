import copy
import numpy as np
import pandas as pd


class Population:
    """
    A class to simulate disease spread through a population of agents belonging to interacting subgroups
    that also interact with an outside source of infections

    Parameters
    ----------
    groups : list of strings
        Population group names
    sizes : dictionary with keys equal to group names and values equal to integers
        Population group sizes
    daily_interactions : nested dictionaries with keys indexed by group names and values equal to floats >=0
        The average number of unique daily interactions by an individual in the first group with an individual in the second group
    interaction_exposure_prob : nested dictionaries with keys indexed by groups and values equal to floats in [0,1]
        The probability that an individual in the first group will become infected due to an interaction with an infected person in the second group
    outside_prevalence : float in [0,1]
        The prevalence of infectious people in outside population (can be adjusted each day)
    outside_daily_interactions : dictionary with keys equal to group names and values equal to floats >=0
        The number of unique daily interactions with a group member and the outside population
    outside_interaction_exposure_prob : dictionary with keys equal to group names and values equal to floats in [0,1]
        The probability of an exposure due to an interaction between a group member and the outside population
    exposed_prevalence : dictionary with keys equal to group names and values equal to floats in [0,1]
        The initial prevalence of exposures in each group
    infected_prevalence : dictionary with keys equal to group names and values equal to floats in [0,1]
        The initial prevalence of infections in each group
    recovered_prevalence : dictionary with keys equal to group names and values equal to floats in [0,1]
        The initial prevalence of past exposure and recovery from the disease in each group
    duration_of_exposed : function which takes as input an integer, n, and returns a numpy array of size (n,) of integers
        The numbers of days the agents stay in the exposed state
    duration_of_infected : function which takes as input an integer, n, and returns a numpy array of size (n,) of integers
        The numbers of days the agents stay in the infected state
    symptomatic_infection : function which takes as input an integer, n, and returns a boolean numpy array of size (n,)
        The probabilities that the agents will develop a symptomatic infection
    symptomatic_infection_delay : function which takes as input an integer, n, and returns a numpy array of size (n,) of integers
        The numbers of days post infection before the agent displays symptoms
    uninfected_daily_symptom_probability : float in [0,1]
        The probability an agent will report a symptom, independent of their health state
    number_of_contacts_reported : function which takes as input an integer, n, and returns a numpy array of size (n,) of integers
        The numbers of contacts the agents will report if they test positive


    """

    def __init__(self, groups=None,
                 sizes=None,
                 daily_interactions=None,
                 interaction_exposure_prob=None,
                 outside_prevalence=None,
                 outside_daily_interactions=None,
                 outside_interaction_exposure_prob=None,
                 exposed_prevalence=None,
                 infected_prevalence=None,
                 recovered_prevalence=None,
                 duration_of_exposed=None,
                 duration_of_infected=None,
                 symptomatic_infection=None,
                 symptomatic_infection_delay=None,
                 uninfected_daily_symptom_probability=None,
                 number_of_contacts_reported=None,
                 **kwargs):

        self.groups = groups
        self.sizes = sizes
        self.total_size = sum(sizes.values())
        self.agents = {group: np.arange(sizes[group]) for group in groups}
        self.daily_interactions = copy.deepcopy(daily_interactions)
        self.original_daily_interactions = copy.deepcopy(daily_interactions)
        self.interaction_exposure_prob = copy.deepcopy(interaction_exposure_prob)
        self.outside_prevalence = copy.deepcopy(outside_prevalence)
        self.outside_daily_interactions = copy.deepcopy(outside_daily_interactions)
        self.original_outside_daily_interactions = copy.deepcopy(outside_daily_interactions)
        self.outside_interaction_exposure_prob = copy.deepcopy(outside_interaction_exposure_prob)
        self.exposed_prevalence = copy.deepcopy(exposed_prevalence)
        self.infected_prevalence = copy.deepcopy(infected_prevalence)
        self.recovered_prevalence = copy.deepcopy(recovered_prevalence)
        self.duration_of_exposed = copy.deepcopy(duration_of_exposed)
        self.duration_of_infected = copy.deepcopy(duration_of_infected)
        self.symptomatic_infection_delay = copy.deepcopy(symptomatic_infection_delay)
        self.symptomatic_infection = copy.deepcopy(symptomatic_infection)
        self.uninfected_daily_symptom_probability = copy.deepcopy(uninfected_daily_symptom_probability)
        self.number_of_contacts_reported = copy.deepcopy(number_of_contacts_reported)
        self.day = 0

        self.max_isolation_duration = 10  # Duration of quarantine in this population
        self.max_quarantine_duration = 10  # Duration of isolation in this population
        self.quarantine_not_interactable = True  # Flag determining if quarantined individuals can interact

        self.time_series = list()
        self.initialize_population()

    def initialize_population(self):
        self.agents_states = {}
        self.quarantined = {}
        self.isolated = {}
        self.testable = {}

        self.agents_days_in_state = {}
        self.agents_duration_of_exposed = {}
        self.agents_duration_of_infected = {}
        self.agents_delay_until_symptomatic = {}
        self.agents_days_in_quarantine = {}
        self.agents_days_in_isolation = {}
        self.agents_days_until_isolation = {}
        self.agents_days_until_quarantine_release = {}
        self.agents_days_until_isolation_release = {}
        self.agents_days_since_exposed = {}
        self.agents_days_since_test = {}
        self.agents_number_of_contacts_reported = {}
        self.agents_will_be_symptomatic = {}
        self.not_infected_with_symptoms = {}
        self.number_positive_tests = {}
        self.number_exposed_from_outside = {}
        self.number_exposed_from_inside = {}
        self.under_lockdown = {}
        self.number_tests_performed = 0

        for group in self.groups:
            self.under_lockdown[group] = False
            self.number_positive_tests[group] = 0

            # Set individual agent disease progression durations and whether they will show symptoms
            self.agents_duration_of_exposed[group] = self.duration_of_exposed[group](self.sizes[group])
            self.agents_duration_of_infected[group] = self.duration_of_infected[group](self.sizes[group])
            self.agents_will_be_symptomatic[group] = self.symptomatic_infection[group](self.sizes[group])
            self.agents_delay_until_symptomatic[group] = self.symptomatic_infection_delay[group](self.sizes[group])
            self.agents_number_of_contacts_reported[group] = self.number_of_contacts_reported[group](self.sizes[group])

            # Initialize all states to susceptible, days in state to 0 and testable status to 1
            self.agents_states[group] = np.array(['s'] * self.sizes[group])
            self.agents_days_in_state[group] = np.zeros(self.sizes[group])
            self.testable[group] = np.ones(self.sizes[group], dtype=bool)

            # Initialize quarantine and isolation status and timers
            self.quarantined[group] = np.zeros(self.sizes[group], dtype=bool)
            self.agents_days_in_quarantine[group] = np.inf * np.ones(self.sizes[group])
            self.agents_days_until_quarantine_release[group] = np.inf * np.ones(self.sizes[group])

            self.isolated[group] = np.zeros(self.sizes[group], dtype=bool)
            self.agents_days_in_isolation[group] = np.inf * np.ones(self.sizes[group])
            self.agents_days_until_isolation[group] = np.inf * np.ones(self.sizes[group])
            self.agents_days_until_isolation_release[group] = np.inf * np.ones(self.sizes[group])

            self.agents_days_since_test[group] = np.inf * np.ones(self.sizes[group])

            # Choose some agents that have already been infected and recovered
            index_to_recovered = np.random.rand(self.sizes[group]) <= self.recovered_prevalence[group]
            self.agents_states[group][index_to_recovered] = 'r'

            # Choose some susceptible agents to expose at random and time in state uniformly at random in [0, days in exposed)
            index_to_expose = self.susceptible[group] & (
                    np.random.rand(self.sizes[group]) <= (self.exposed_prevalence[group] / (self.susceptible[group].sum()/self.sizes[group])))
            self.agents_states[group][index_to_expose] = 'e'
            self.agents_days_in_state[group][index_to_expose] = np.random.randint(0, self.agents_duration_of_exposed[
                group][index_to_expose])
            self.agents_days_since_exposed[group] = -np.inf * np.ones(self.sizes[group], dtype=int)
            self.agents_days_since_exposed[group][index_to_expose] = self.agents_days_in_state[group][index_to_expose]

            # Choose some agents to infect and set days in state uniformly at random from [0, days in infectious)
            index_to_infect = self.susceptible[group] & (
                    np.random.rand(self.sizes[group]) <= (self.infected_prevalence[group] / (self.susceptible[group].sum()/self.sizes[group])))
            self.agents_states[group][index_to_infect] = 'i'
            self.agents_days_in_state[group][index_to_infect] = np.random.randint(0, self.agents_duration_of_infected[
                group][index_to_infect])

            self.number_exposed_from_outside[group] = index_to_expose.sum() + index_to_infect.sum()
            self.number_exposed_from_inside[group] = 0

            # Set some initial not infected and symptomatic
            self.not_infected_with_symptoms[group] = np.invert(self.infected[group]) & (
                        np.random.rand(self.sizes[group]) <= self.uninfected_daily_symptom_probability[group])

        self.record_time_series()

    @property
    def outside_exposure_probability(self):
        """
        A dictionary with keys equal to group names and values equal to floats specifying probability each day of an
        agent contracting disease from interactions with the community outside the modelled population
        """
        #return {group: self.outside_prevalence * self.outside_daily_interactions[group] *
        #               self.outside_interaction_exposure_prob[group] for group in self.groups}
        return {group: 1 - (1 - self.outside_prevalence * self.outside_interaction_exposure_prob[group]) ** self.outside_daily_interactions[group] for group in self.groups}

    @property
    def susceptible(self):
        return {group: self.agents_states[group] == 's' for group in self.agents_states.keys()}

    @property
    def exposed(self):
        return {group: self.agents_states[group] == 'e' for group in self.agents_states.keys()}

    @property
    def infected(self):
        return {group: self.agents_states[group] == 'i' for group in self.agents_states.keys()}

    @property
    def has_symptoms(self):
        return {group: self.not_infected_with_symptoms[group]
                       | (self.infected[group] & self.agents_will_be_symptomatic[group] & (
                    self.agents_days_in_state[group] >= self.agents_delay_until_symptomatic[group])) for group in
                self.agents_states.keys()}

    @property
    def recovered(self):
        return {group: self.agents_states[group] == 'r' for group in self.agents_states.keys()}

    @property
    def unquarantined(self):
        return {group: np.invert(self.quarantined[group]) for group in self.groups}

    @property
    def unisolated(self):
        return {group: np.invert(self.isolated[group]) for group in self.groups}

    @property
    def interactable(self):
        if self.quarantine_not_interactable:
            return {group: self.unquarantined[group] & self.unisolated[group] for group in self.groups}
        else:
            return {group: self.unisolated[group] for group in self.groups}

    @property
    def number_susceptible(self):
        return {group: np.sum(self.susceptible[group]) for group in self.groups}

    @property
    def number_exposed(self):
        return {group: np.sum(self.exposed[group]) for group in self.groups}

    @property
    def number_infected(self):
        return {group: np.sum(self.infected[group]) for group in self.groups}

    @property
    def number_recovered(self):
        return {group: np.sum(self.recovered[group]) for group in self.groups}

    @property
    def number_quarantined(self):
        return {group: np.sum(self.quarantined[group]) for group in self.groups}

    @property
    def number_isolated(self):
        return {group: np.sum(self.isolated[group]) for group in self.groups}

    @property
    def number_interactable(self):
        return {group: np.sum(self.interactable[group]) for group in self.groups}

    @property
    def number_unisolated(self):
        return {group: self.sizes[group] - self.number_isolated[group] for group in self.groups}

    @property
    def number_infected_and_interactable(self):
        return {group: np.sum(self.infected[group] & self.interactable[group]) for group in self.groups}

    @property
    def time_series_df(self):
        return pd.DataFrame(self.time_series)

    def perform_tests(self, testing_strategy, param_dict):
        testing_strategy(self, **param_dict)
        return None

    def release_agents(self, release_strategy, param_dict):
        release_strategy(self, **param_dict)
        return None

    def trace_contacts(self, trace_strategy, param_dict):
        trace_strategy(self, **param_dict)
        return None

    def activate_lockdown(self, groups=None, interaction_reduction_percentage=1.0):
        if groups is None:
            groups = self.groups
        for group1 in groups:
            if not self.under_lockdown[group1]:
                self.under_lockdown[group1] = True
                self.outside_daily_interactions[group1] *= (1.0 - interaction_reduction_percentage)
                for group2 in groups:
                    self.daily_interactions[group1][group2] *= (1.0 - interaction_reduction_percentage)
                    self.daily_interactions[group2][group1] *= (1.0 - interaction_reduction_percentage)
        return None

    def deactivate_lockdown(self, groups=None):
        if groups is None:
            groups = self.groups
        for group1 in groups:
            if self.under_lockdown[group1]:
                self.under_lockdown[group1] = False
                self.outside_daily_interactions[group1] = copy.deepcopy(
                    self.original_outside_daily_interactions[group1])
                for group2 in groups:
                    self.daily_interactions[group1][group2] = copy.deepcopy(
                        self.original_daily_interactions[group1][group2])
                    self.daily_interactions[group2][group1] = copy.deepcopy(
                        self.original_daily_interactions[group2][group1])
        return None

    def isolate_agents(self):
        # Isolate agents that had a positive test when the test results become available
        for group in self.groups:
            index_to_isolate = self.agents_days_until_isolation[group] <= 0
            self.isolated[group][index_to_isolate] = True
            self.quarantined[group][index_to_isolate] = False
            self.agents_days_in_isolation[group][index_to_isolate] = 0
            self.agents_days_in_quarantine[group][index_to_isolate] = np.inf
            self.agents_days_until_isolation[group][index_to_isolate] = np.inf
            self.agents_days_until_isolation_release[group][index_to_isolate] = self.max_isolation_duration
            self.agents_days_until_quarantine_release[group][index_to_isolate] = np.inf
            self.number_positive_tests[group] = np.sum(index_to_isolate)

        return None

    def release_from_isolation(self):
        # Release agents whose isolation countdown has reached 0
        for group in self.groups:
            index_to_release = self.agents_days_until_isolation_release[group] <= 0
            self.isolated[group][index_to_release] = False
            self.agents_days_in_isolation[group][index_to_release] = np.inf
            self.agents_days_until_isolation_release[group][index_to_release] = np.inf
        return None

    def release_from_quarantine(self):
        # Release agents whose quarantine countdown has reached 0
        for group in self.groups:
            index_to_release = self.agents_days_until_quarantine_release[group] <= 0
            self.quarantined[group][index_to_release] = False
            self.agents_days_in_quarantine[group][index_to_release] = np.inf
            self.agents_days_until_quarantine_release[group][index_to_release] = np.inf
        return None

    def spike_exposures(self, num_exposures=None):
        # Add specified numbers of exposures to each group in the population, assumes exposures from inside population
        for group in self.groups:
            index_exposable = np.where(self.interactable[group] & self.susceptible[group])[0]
            index_to_expose = np.zeros(self.sizes[group], dtype=bool)
            index_to_expose[np.random.choice(index_exposable, size=min(len(index_exposable), num_exposures[group]),
                                             replace=False)] = True
            self.agents_states[group][index_to_expose] = 'e'
            self.agents_days_in_state[group][index_to_expose] = 0
            self.number_exposed_from_inside[group] = self.number_exposed_from_inside[group] + index_to_expose.sum()
        return None

    def step(self):
        # Initialize next states
        next_states = copy.deepcopy(self.agents_states)

        # Increment agent day counters
        self.day = self.day + 1
        for group in self.groups:
            self.agents_days_since_exposed[group] += 1
            self.agents_days_since_test[group] += 1
            self.agents_days_in_state[group] += 1
            self.agents_days_in_quarantine[group] += 1
            self.agents_days_in_isolation[group] += 1
            self.agents_days_until_isolation[group] -= 1
            self.agents_days_until_isolation_release[group] -= 1
            self.agents_days_until_quarantine_release[group] -= 1

        # Expose agents from outside interactions with broader community
        for group in self.groups:
            # Add additional infections to population from outside sources
            index_to_expose = self.susceptible[group] & self.interactable[group] & (
                    np.random.rand(self.sizes[group]) <= self.outside_exposure_probability[group])
            next_states[group][index_to_expose] = 'e'
            self.agents_days_in_state[group][index_to_expose] = 0
            self.number_exposed_from_outside[group] = self.number_exposed_from_outside[group] + index_to_expose.sum()

        # Move agents through disease states and into and out of isolation and quarantine
        for group in self.groups:
            # Move exposed people to infected
            index_to_infect = self.exposed[group] & (
                    self.agents_days_in_state[group] == self.agents_duration_of_exposed[group])
            next_states[group][index_to_infect] = 'i'
            self.agents_days_in_state[group][index_to_infect] = 0

            # Move infected people to recovered
            index_to_recover = self.infected[group] & (
                    self.agents_days_in_state[group] == self.agents_duration_of_infected[group])
            next_states[group][index_to_recover] = 'r'
            self.agents_days_in_state[group][index_to_recover] = 0

            # Randomly choose some not infected to develop symptoms today
            self.not_infected_with_symptoms[group] = np.invert(self.infected[group]) & (
                    np.random.rand(self.sizes[group]) <= self.uninfected_daily_symptom_probability[group])

            # Isolate agents when their positive test results become available
            self.isolate_agents()

            # Release agents from isolation if their isolation release countdown hits 0
            self.release_from_isolation()

            # Release agents from quarantine if their quarantine release countdown hits 0
            self.release_from_quarantine()

        # Spread infections between members of the population
        for group1 in self.groups:
            for group2 in self.groups:
                daily_interactions = self.daily_interactions[group1][group2]
                interact_exposure_prob = self.interaction_exposure_prob[group1][group2]

                # Group 1 new infections due to interaction with infected group 2
                group1_infect_prob = 1 - ((1 - interact_exposure_prob * self.number_infected_and_interactable[group2] /
                                           self.number_interactable[group2]) ** daily_interactions)
                index_to_expose = self.susceptible[group1] & self.interactable[group1] & (
                        np.random.rand(self.sizes[group1]) <= group1_infect_prob)
                next_states[group1][index_to_expose] = 'e'
                self.agents_days_in_state[group1][index_to_expose] = 0
                self.number_exposed_from_inside[group1] = self.number_exposed_from_inside[
                                                              group1] + index_to_expose.sum()

        # Set counter on days since exposed for new exposures
        for group in self.groups:
            index_exposed = (self.agents_states[group] != 'e') & (next_states[group] == 'e')
            self.agents_days_since_exposed[group][index_exposed] = 0

        # Update states and record counts
        self.agents_states = next_states
        self.record_time_series()

    def record_time_series(self):
        """
        Record the time series on each step in narrow format: time, var, group, count
        """

        # These states must have corresponding property in this class
        vars_to_save = ['number_susceptible', 'number_exposed',
                        'number_infected', 'number_recovered',
                        'number_isolated', 'number_quarantined',
                        'number_positive_tests',
                        'number_exposed_from_outside', 'number_exposed_from_inside']

        records = []
        for var in vars_to_save:
            if hasattr(self, var):
                count_dict = getattr(self, var)
                for group, count in count_dict.items():
                    record = {'time': self.day,
                              'var': var.replace('number_', ''),
                              'group': group,
                              'count': count}
                    records.append(record)

        self.time_series.extend(records)

    def concatenate_groups(self, groups, sizes, attribute_dict):
        """
        Consistently concatenate agent attribute vectors into a single vector

        Parameters
        ----------
        groups : Population.groups list containing group names
        sizes : Population.sizes dictionary, keyed by groups
        attribute_dict : Population.* agent attribute dictionary, keyed by groups

        Returns
        -------
        attribute : numpy array of agent attributes concatenated in order of groups
        """
        total_size = 0
        for group in groups:
            total_size += sizes[group]

        attribute = np.empty(total_size, dtype=attribute_dict[groups[0]].dtype)

        start = 0
        for group in groups:
            end = start + sizes[group]
            attribute[start:end] = attribute_dict[group]
            start = end

        return attribute

    def unconcatenate_groups(self, groups, sizes, attribute):
        """
        Unconcatenate output of Population.concatenate_groups() into an attribute dictionary, keyed by groups

        Parameters
        ----------
        groups : Population.groups list containing group names
        sizes : Population.sizes dictionary, keyed by groups
        attribute : numpy array output by Population.concatenate_groups()

        Returns
        -------
        attribute_dict : dictionary of agent attributes keyed by groups
        """
        attribute_dict = {}
        start = 0
        for group in groups:
            end = start + sizes[group]
            attribute_dict[group] = attribute[start:end]
            start = end

        return attribute_dict

"""
Functions for performing pre-defined simulations over a fixed time period with fixed intervention strategies
"""
from model.population import Population
import model.strategies as strats
import psutil
from joblib import Parallel, delayed
import pandas as pd

def run_simulation_multicore(simulation=None, population_params=None, num_days=None, num_sims=None, num_jobs=-1):
    """
    Perform many simulations in parallel for a fixed number of days with a fixed set of interventions on a population
    defined by a fixed set of parameters.
    Parameters
    ----------
    simulation : A callable of the form f(population_params, num_days)
    population_params : a population parameter dictionary
        Dictionay object encoding required population parameters needed to instantiate a Population object
    num_days : int >= 1
        The number of days for each simulation
    num_sims : int >= 1
        The number of simulations to perform
    num_jobs : int >= 1 (or -1)
        The number of cores to use (Default: -1 for maximum available)

    Returns
    -------
    simulated_populations : a list of num_sim Population objects
    """
    if num_jobs is not None:
        if num_jobs == -1:
            num_jobs = psutil.cpu_count(logical=False)
    else:
        num_jobs = 1

    simulated_populations = Parallel(n_jobs=num_jobs)(
        delayed(simulation)(population_params, num_days) for _ in range(num_sims))

    return simulated_populations


def example_simulation(population_params, num_days):
    # Initialize a population
    pop = Population(**population_params)

    # Test the entire population initially and isolate positive cases
    pop.perform_tests(strats.test_everyone, {'test_fnr': 0.01, 'test_fpr': .001, 'test_results_delay': 0})
    pop.isolate_agents()
    pop.record_time_series()

    # Simulate disease progression assuming weekly surveillance testing and quarantining of contacts
    for day in range(num_days):
        pop.step()
        pop.perform_tests(strats.pooled_surveillence_test, {'days_between_tests': 7,
                               'pool_size': 5, 'pooled_test_fnr': 0.01, 'pooled_test_fpr': 0.001,
                               'test_fnr': 0.01, 'test_fpr': 0.001, 'test_results_delay': 1})
        pop.trace_contacts(strats.basic_contact_trace, {'contact_trace_efficacy': .15})

    return pop



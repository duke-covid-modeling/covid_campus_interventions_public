
import numpy as np

"""
A testing strategy is a function which takes as input a Population object (and some set of parameters) and at least 
updates the following Population attributes:
    - population.agents_days_since_test
    - population.agents_days_until_isolation
    - population.number_tests_performed
"""

def test_everyone(population, test_fnr=0.01, test_fpr=0.001, test_results_delay=0):
    """
    Test the entire population.

    Parameters
    ----------
    population : An instance of a Population class
    test_results_delay : int >= 0
        The number of days before test results become available

    Notes
    -----
    This can be used to model initial testing a student body at the start of the semesterupon return to campus by
    setting test results delay to 0 to ensure isolations occur before anyone identified as infected has a chance to
    expose others
    """
    for group in population.groups:
        # Test everyone
        index_to_test = np.ones(population.sizes[group], dtype=bool)

        # Update number of tests performed
        population.number_tests_performed = population.number_tests_performed + np.sum(index_to_test)

        # Start the isolation countdown for those agents that test positive
        index_tested_positive = index_to_test & (
                (population.infected[group] & (np.random.rand(population.sizes[group]) <= (1 - test_fnr)))
                | (np.invert(population.infected[group]) & (np.random.rand(population.sizes[group]) <= test_fpr)))

        # Retain existing countdowns that are less than test_results_delay
        population.agents_days_until_isolation[group][index_tested_positive] = np.minimum(
            population.agents_days_until_isolation[group][index_tested_positive],
            test_results_delay * np.ones(np.sum(index_tested_positive)))

        # Anyone who tests positive will eventually be isolated, and assumed to have
        # eventually recovered and so will not be tested again
        population.testable[group][index_tested_positive] = 0


def basic_fixed_time_between_tests_strategy(population, days_between_tests=7, test_fnr=0.01, test_fpr=0.001,
                                            test_results_delay=1):
    """
    Basic testing strategy in which a roughly equal number of agents are tested each day, with each member of the entire
    population being tested once during each days_between_tests days.

    Parameters
    ----------
    population : an instance of a Population class
    days_between_tests : int >= 1
        The number of days required to test the entire population exactly once
    test_fnr : float in [0,1]
        The test false negative rate
    test_fpr : float in float in [0,1]
        The test false positive rate
    test_results_delay : int >= 0
        The number of days before test results are available
    """
    for group in population.groups:
        # test everyone that is currently not in isolation or quarantine and is due for a test on this day
        index_to_test = population.interactable[group] & population.testable[group] & (
                    np.mod(population.agents[group], days_between_tests) == np.mod(population.day, days_between_tests))

        # update number of tests performed
        population.number_tests_performed = population.number_tests_performed + np.sum(index_to_test)

        # update agents_days_since_test
        population.agents_days_since_test[group][index_to_test] = 0

        # start the isolation countdown for those agents that test postive
        index_tested_positive = index_to_test & (
                    (population.infected[group] & (np.random.rand(population.sizes[group]) <= (1 - test_fnr)))
                    | (np.invert(population.infected[group]) & (np.random.rand(population.sizes[group]) <= test_fpr)))

        # Retain existing countdowns that are less than test_results_delay
        population.agents_days_until_isolation[group][index_tested_positive] = np.minimum(
            population.agents_days_until_isolation[group][index_tested_positive],
            test_results_delay * np.ones(np.sum(index_tested_positive)))

        # Anyone who tests positive will eventually be isolated, and assumed to have
        # eventually recovered and so will not be tested again
        population.testable[group][index_tested_positive] = 0

    return None


def pooled_surveillence_test(population, days_between_tests=7,
                             pool_size=5, pooled_test_fnr=0.01, pooled_test_fpr=0.001,
                             test_fnr=0.01, test_fpr=0.001, test_results_delay=1):
    """
    Pooled testing strategy in which a roughly equal number of agents are tested each day, with each member of the
    entire population being tested once during each days_between_tests days, with tests first being pooled into
    pools of a specified size.

    Parameters
    ----------
    population : an instance of a Population class
    days_between_tests : int >= 1
        The number of days required to test the entire population exactly once
    pool_size : int >= 1
        The number of tests to pool together
    pooled_test_fnr : float in [0,1]
        The pooled test false negative rate
    pooled_test_fpr : float in [0,1]
        The pooled test false positive rate
    test_fnr : float in [0,1]
        The follow-up individual test false negative rate
    test_fpr : float in float in [0,1]
        The follow-up individual test false positive rate
    test_results_delay : int >= 0
        The number of days before test results are available
    """
    # concatenate agent attribute vectors across all groups
    groups = population.groups
    sizes = population.sizes
    agents = population.concatenate_groups(groups, sizes, population.agents)
    testable = population.concatenate_groups(groups, sizes, population.testable)
    interactable = population.concatenate_groups(groups, sizes, population.interactable)
    infected = population.concatenate_groups(groups, sizes, population.infected)
    agents_days_until_isolation = population.concatenate_groups(groups, sizes, population.agents_days_until_isolation)
    agents_days_since_test = population.concatenate_groups(groups, sizes, population.agents_days_since_test)

    # determine the agents to test
    index_to_test = interactable & testable & (
                np.mod(agents, days_between_tests) == np.mod(population.day, days_between_tests))

    # update agents_days_since_test
    agents_days_since_test[index_to_test] = 0

    # initialize testing pools
    num_pools = int(np.sum(index_to_test) / pool_size) + 1
    pools = np.array(range(num_pools))
    pool_infected = np.zeros(len(pools), dtype=bool)

    # loop over pools and assign agents to be tested and infection status of pools
    not_assigned = np.ones(population.total_size, dtype=bool)
    pool_assignment = np.empty(population.total_size, dtype=int)

    for pool in pools:
        possible_assignees = np.where(index_to_test & not_assigned)[0]
        assignees = np.random.choice(possible_assignees, min(pool_size, len(possible_assignees)), replace=False)
        pool_assignment[assignees] = pool
        not_assigned[assignees] = False
        pool_infected[pool] = np.any(infected[assignees])

    # perform pooled tests
    pool_tested_positive = ((pool_infected & (np.random.rand(num_pools) <= (1 - pooled_test_fnr)))
                           | (np.invert(pool_infected) & (np.random.rand(num_pools) <= pooled_test_fpr)))

    # update number of tests performed
    population.number_tests_performed = population.number_tests_performed + len(pools)

    # perform individual follow-up tests
    for positive_pool in np.where(pool_tested_positive)[0]:
        index_to_retest = index_to_test & (pool_assignment == positive_pool)
        index_tested_positive = index_to_retest & (
                (infected & (np.random.rand(population.total_size) <= (1 - test_fnr)))
                | (np.invert(infected) & (np.random.rand(population.total_size) <= test_fpr)))

        # update number of tests performed
        population.number_tests_performed = population.number_tests_performed + np.sum(index_to_retest)

        # Retain existing countdowns that are less than test_results_delay
        agents_days_until_isolation[index_tested_positive] = np.minimum(
            agents_days_until_isolation[index_tested_positive],
            test_results_delay * np.ones(np.sum(index_tested_positive)))

        # Anyone who tests positive will eventually be isolated, and assumed to have
        # eventually recovered and so will not be tested again
        testable[index_tested_positive] = 0

    # update population attributes
    population.testable = population.unconcatenate_groups(groups, sizes, testable)
    population.agents_days_until_isolation = population.unconcatenate_groups(groups, sizes, agents_days_until_isolation)
    population.agents_days_since_test = population.unconcatenate_groups(groups, sizes, agents_days_since_test)

    return None


def test_symptomatic(population, test_fnr=0.01, test_fpr=0.001, test_results_delay=1):
    """
    Testing of those agents in the population that are currently symptomatic.

    Parameters
    ----------
    population : an instance of a Population class
    test_fnr : float in [0,1]
        The test false negative rate
    test_fpr : float in float in [0,1]
        The test false positive rate
    test_results_delay : int >= 0
        The number of days before test results are available
    """
    groups = population.groups

    for group in groups:
        # only test symptomatic people that weren't already tested today
        index_to_test = (population.agents_days_since_test[group] > 0) & population.has_symptoms[group]

        # update number of tests performed
        population.number_tests_performed = population.number_tests_performed + np.sum(index_to_test)

        # run tests
        index_tested_positive = index_to_test & (
                (population.infected[group] & (np.random.rand(population.sizes[group]) <= (1 - test_fnr)))
                | (np.invert(population.infected[group]) & (np.random.rand(population.sizes[group]) <= test_fpr)))

        # Retain existing countdowns that are less than test_results_delay
        population.agents_days_until_isolation[group][index_tested_positive] = np.minimum(
            population.agents_days_until_isolation[group][index_tested_positive],
            test_results_delay * np.ones(np.sum(index_tested_positive)))

        # Anyone who tests positive will eventually be isolated, and assumed to have
        # eventually recovered and so will not be tested again
        population.testable[group][index_tested_positive] = 0

    return None


def test_quarantine(population, time_till_test=5, test_fnr=.01, test_fpr=.001, test_results_delay=1):
    """
    A quarantine testing strategy in which quarantined people are tested after a fixed number of days and either
    moved to isolation if the test if positive or set to be released if the test is negative.

    Parameters
    ----------
    population : an instance of a Population class
    time_till_test : int >= 0
    test_fnr : float in [0,1]
        The test false negative rate
    test_fpr : float in float in [0,1]
        The test false positive rate
    test_results_delay : int >= 0
        The number of days before test results are available
    """
    for group in population.groups:
        index_to_test = population.quarantined[group] & (population.agents_days_in_quarantine[group] >= time_till_test)

        # update number of tests performed
        population.number_tests_performed = population.number_tests_performed + np.sum(index_to_test)

        # run tests
        index_tested_positive = index_to_test & (
                (population.infected[group] & (np.random.rand(population.sizes[group]) <= (1 - test_fnr)))
                | (np.invert(population.infected[group]) & (np.random.rand(population.sizes[group]) <= test_fpr)))

        # Retain existing countdowns that are less than test_results_delay
        population.agents_days_until_isolation[group][index_tested_positive] = np.minimum(
            population.agents_days_until_isolation[group][index_tested_positive],
            test_results_delay * np.ones(np.sum(index_tested_positive)))

        # Anyone who tests positive will eventually be isolated, and assumed to have
        # eventually recovered and so will not be tested again
        population.testable[group][index_tested_positive] = 0

        # set timer to release agents that tested negative
        index_tested_negative = index_to_test & np.invert(index_tested_positive)
        population.agents_days_until_quarantine_release[group][index_tested_negative] = np.minimum(
            population.agents_days_until_quarantine_release[group][index_tested_negative],
            test_results_delay * np.ones(np.sum(index_tested_negative)))

    return None


def test_contacts(population, test_fnr=.01, test_fpr=.001, test_results_delay=1):
    """
    An alternative to quarantining contacts, instead test those flagged as contacts who would have been quarantined.
    As usual, if a contact tests positive, they are moved to isolation.

    Parameters
    ----------
    population : an instance of a Population class
    test_fnr : float in [0,1]
        The test false negative rate
    test_fpr : float in float in [0,1]
        The test false positive rate
    test_results_delay : int >= 0
        The number of days before test results are available
    """
    for group in population.groups:
        # Test everyone in "quarantine"
        index_to_test = population.quarantined[group]

        # Update number of tests performed
        population.number_tests_performed = population.number_tests_performed + np.sum(index_to_test)

        # Run tests
        index_tested_positive = index_to_test & (
                (population.infected[group] & (np.random.rand(population.sizes[group]) <= (1 - test_fnr)))
                | (np.invert(population.infected[group]) & (np.random.rand(population.sizes[group]) <= test_fpr)))

        # Retain existing countdowns that are less than test_results_delay
        population.agents_days_until_isolation[group][index_tested_positive] = np.minimum(
            population.agents_days_until_isolation[group][index_tested_positive],
            test_results_delay * np.ones(np.sum(index_tested_positive)))

        # Anyone who tests positive will eventually be isolated, and assumed to have
        # eventually recovered and so will not be tested again
        population.testable[group][index_tested_positive] = 0

    return None


"""
A contact tracing strategy is a function which takes as input a Population object (and some set of parameters) 
and at least updates the following Population attributes:
    - population.quarantined
    - population.agents_days_in_quarantined
"""


def basic_contact_trace(population, contact_trace_efficacy=0.5):
    """
    A contact trace strategy in which at most X% of contacts are taken from exposed/infected based on a chosen
    contact trace efficacy.

    Parameters
    ----------
    population : an instance of a Population class
    contact_trace_efficacy: float in [0,1]
        The maximum fraction of contacts reported by a positive case which were actually previously exposed/infected
    """
    for group in population.groups:
        # determine the total number of agents to trace
        positive_test_today = population.agents_days_in_isolation[group] <= 0
        number_of_contacts_to_trace = population.agents_number_of_contacts_reported[group][positive_test_today]
        number_of_exposed_to_trace = np.round((np.sum(number_of_contacts_to_trace) * contact_trace_efficacy)).astype(int)

        # select a group of exposed/infected agents to trace
        potential_exposed_to_trace = population.interactable[group] & (population.exposed[group] | population.infected[group])

        exposed_traced = np.random.choice(np.where(potential_exposed_to_trace)[0],
                                          size=min(number_of_exposed_to_trace, np.sum(potential_exposed_to_trace)),
                                          replace=False)

        # select a group of susceptible agents to trace
        number_of_susceptible_to_trace = np.sum(number_of_contacts_to_trace) - min(number_of_exposed_to_trace, np.sum(potential_exposed_to_trace))
        potential_susceptible_to_trace = population.interactable[group] & (population.susceptible[group] | population.recovered[group])
        susceptible_traced = np.random.choice(np.where(potential_susceptible_to_trace)[0],
                                              size=min(number_of_susceptible_to_trace, np.sum(potential_susceptible_to_trace)),
                                              replace=False)

        # quarantine the agents traced
        index_to_quarantine = np.zeros(population.sizes[group], dtype=bool)
        index_to_quarantine[exposed_traced] = True
        index_to_quarantine[susceptible_traced] = True

        population.quarantined[group][index_to_quarantine] = True
        population.agents_days_in_quarantine[group][index_to_quarantine] = 0
        population.agents_days_until_quarantine_release[group][index_to_quarantine] = population.max_quarantine_duration

    return None


def random_contact_trace(population):
    """
    A contact trace strategy in which contacts are taken uniformly at random from the population

    Parameters
    ----------
    population : an instance of a Population class
    """
    for group in population.groups:
        # determine the total number of agents to trace
        positive_test_today = population.agents_days_in_isolation[group] <= 0
        number_of_contacts_to_trace = population.agents_number_of_contacts_reported[group][positive_test_today]
       
        # select a group of agents to trace
        potential_exposed_to_trace = population.interactable[group]

        exposed_traced = np.random.choice(np.where(potential_exposed_to_trace)[0],
                                          size=min(np.sum(number_of_contacts_to_trace), np.sum(potential_exposed_to_trace)),
                                          replace=False)

        # quarantine the agents traced
        index_to_quarantine = np.zeros(population.sizes[group], dtype=bool)
        index_to_quarantine[exposed_traced] = True
        print(index_to_quarantine.sum())
        
        population.quarantined[group][index_to_quarantine] = True
        population.agents_days_in_quarantine[group][index_to_quarantine] = 0
        population.agents_days_until_quarantine_release[group][index_to_quarantine] = population.max_quarantine_duration

    return None
